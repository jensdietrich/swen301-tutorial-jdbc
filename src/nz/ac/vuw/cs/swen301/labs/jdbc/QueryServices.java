package nz.ac.vuw.cs.swen301.labs.jdbc;

import java.util.ServiceLoader;

public class QueryServices {

    public static void main (String[] args) throws Exception {
        java.util.ServiceLoader<java.sql.Driver> drivers = java.util.ServiceLoader.load(java.sql.Driver.class);
        for (java.sql.Driver driver:drivers) {
            System.out.println(driver);
        }
    }
}
