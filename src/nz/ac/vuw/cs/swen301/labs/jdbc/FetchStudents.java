package nz.ac.vuw.cs.swen301.labs.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;

/**
 * Scripts to fetch student data from a CSV file using a JDBC driver.
 * @author jens dietrich
 */
public class FetchStudents {

    public static void main(String[] args) throws Exception {

        fetchAll();

        fetchById(2);
        fetchById(3);

        fetchByDegreeAndMajor("BE","SE");

    }

    public static void fetchAll() throws Exception {
        System.out.println("Fetch all");
        doFetch("SELECT * FROM students");
    }

    public static void fetchById(int id) throws Exception {
        System.out.println("Fetch student with id " + id);
        doFetch("SELECT * FROM students WHERE ID = \'" + id + "\'");
    }

    public static void fetchByDegreeAndMajor(String degree,String major) throws Exception {
        System.out.println("Fetch student with degree " + degree + " and major " + " major");
        doFetch("SELECT * FROM students WHERE DEGREE = \'" + degree + "\' AND MAJOR = \'" + major + "\'");
    }

    private static void doFetch (String sql) throws Exception {
        Connection connection  = DriverManager.getConnection("jdbc:relique:csv:db?separator=,&fileExtension=.csv");
        ResultSet result = connection.createStatement().executeQuery(sql);
        while (result.next()) {
            System.out.print(result.getString("ID"));
            System.out.print("\t");
            System.out.print(result.getString("FIRSTNAME"));
            System.out.print("\t");
            System.out.print(result.getString("LASTNAME"));
            System.out.print("\t");
            System.out.print(result.getString("DEGREE"));
            System.out.print("\t");
            System.out.print(result.getString("MAJOR"));
            System.out.println();
        }
    }



}
